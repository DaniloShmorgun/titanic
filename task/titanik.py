import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe() 
    df['Title'] = df['Name'].str.extract(r' (Mr\.|Mrs\.|Miss\.)')

    mask_dict = {
        'Mr.': (df['Title'] == 'Mr.'),
        'Mrs.': (df['Title'] == 'Mrs.'),
        'Miss.': (df['Title'] == 'Miss.')
    }

    result = []

    for title, mask in mask_dict.items():
        median_age = df[mask]['Age'].median()
        df.loc[mask & df['Age'].isna(), 'Age'] = median_age
        missing_count = df[mask].isna().sum().sum()
        result.append((title, missing_count, round(median_age)))
        
    return result
